from django.db import models
from django.conf import settings
from authentication.models import User
# Create your models here.

class Barang(models.Model):
    penjual = models.ForeignKey(User, on_delete= models.CASCADE, null = True)
    namaBarang = models.CharField(blank = False, max_length=30)
    idBarang = models.CharField(blank = False, max_length=30, unique=True)
    jenisBarang = models.CharField(blank = False, max_length=30)
    harga = models.IntegerField(blank=False)
    status = models.CharField(blank = False, max_length=30)
    ukuran = models.CharField(blank = False, max_length=30)
    kondisi = models.CharField(blank = False, max_length=30)
    is_terjual = models.BooleanField('Is Terjual', default = False)
    pembeli = models.CharField(null=True, max_length=30)
    def __str__(self):
        return self.idBarang