from django.urls import path
from . import views
app_name = 'transaksi'

urlpatterns = [
    path('createTransaksi/', views.createTransaksi, name='createTransaksi'),
    path('listTransaksi/', views.readTransaksi, name = 'readTransaksi'),
]