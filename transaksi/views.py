from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import FormTransaksi
from django.conf import settings
from .models import Transaksi
from barang.models import Barang
# Create your views here.

def createTransaksi(request):
    if request.user.is_authenticated:
        msg = None
        form = FormTransaksi(request.POST or None)
        if request.GET.get('idBarang') is not None:
            hasil = Barang.objects.filter(idBarang = request.GET.get('idBarang')).first()
            jenisBar = hasil.jenisBarang
            price = hasil.harga
        if Transaksi.objects.all().last() is None:
            id = 1
        else:
            val = Transaksi.objects.all().last()
            id = int(val.idTransaksi) + 1
        form.initial['idTransaksi'] = id
        form.initial['jenisBarang'] = jenisBar
        form.initial['ongkosKirim'] = 10000
        form.initial['totalHarga'] = 10000 + price
        if request.method == 'POST':
            if form.is_valid:
                try:
                    hasil = request.POST.get('idBarang')
                    temp = Transaksi()
                    temp.idTransaksi = id
                    temppem = request.user
                    temp.pembeli = temppem
                    temp.metodePembayaran = request.POST['metodePembayaran']
                    temp.totalHarga = int(request.POST.get('price')) + 10000
                    temp.ongkosKirim = 10000
                    temp.jenisBarang = request.POST.get('jenisBar')
                    temp.save()
                    Barang.objects.filter(idBarang = hasil).update(is_terjual = True)
                    Barang.objects.filter(idBarang = hasil).update(pembeli = request.user.nama)
                    msg = 'Barang berhasil dibeli!'
                    return redirect('/transaksi/listTransaksi/')
                except:
                    msg = 'gagal'
                return render(request, 'createTransaksi.html', {'form' : form, 'msg':msg , 'hasil' : hasil, 'jenisBar' : jenisBar, 'price' :price})
            else:
                msg = 'Data kurang lengkap atau salah!'
        return render(request, 'createTransaksi.html', {'form' : form, 'msg' : msg, 'hasil' : hasil, 'jenisBar' : jenisBar, 'price' :price})
    else:
        return redirect('/')

def readTransaksi(request):
    if request.user.is_authenticated:
        p = request.user
        listTransaksi = Transaksi.objects.filter(pembeli = p)
        return render(request, 'readTransaksi.html',{'listTransaksi' : listTransaksi})
    else:
        return redirect('/')
