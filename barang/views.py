from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import FormBarang, UpdateForm
from django.conf import settings
from .models import Barang

# Create your views here.
def createBarang(request):
    if request.user.is_authenticated:
        msg = None
        form = FormBarang(request.POST or None)
        if Barang.objects.all().last() is None:
            id = 1
        else:
            val = Barang.objects.all().last()
            id = int(val.idBarang) + 1
        form.initial['idBarang'] = id
        if request.method == 'POST':
            if form.is_valid:
                try:
                    temp = Barang()
                    temp.namaBarang = request.POST['namaBarang']
                    tempPen = request.user
                    temp.penjual = tempPen
                    temp.idBarang = id
                    temp.jenisBarang = request.POST['jenisBarang']
                    temp.harga = request.POST['harga']
                    temp.status = request.POST['status']
                    temp.ukuran = request.POST['ukuran']
                    temp.kondisi = request.POST['kondisi']
                    temp.save()
                    msg = 'Barang berhasil dijual!'
                    return redirect('/barang/listBarang')
                except:
                    msg = 'Gagal'
                    return render(request, 'createBarang.html', {'form' : form, 'msg':msg})
            else:
                msg = 'Data kurang lengkap atau salah!'
        return render(request, 'createBarang.html', {'form' : form, 'msg':msg})
    else:
        return redirect('/')

def readBarang(request):
    if request.user.is_authenticated:
        listB = Barang.objects.all()
        p = request.user
        listBarang = Barang.objects.filter(penjual = p)
        return render(request, 'readBarang.html',{'listBarang' : listBarang, 'listB' : listB})
    else:
        return redirect('/')

def updateBarang(request, pk):
    if request.user.is_authenticated:
        msg = None
        hasil = Barang.objects.filter(idBarang = pk).first()
        #print(barangs)
        form = UpdateForm(instance = hasil)
        # form.initial['idBarang'] = hasil[0].idBarang
        # form.initial['namaBarang'] = hasil[0].namaBarang
        # form.initial['jenisBarang'] = hasil[0].jenisBarang
        # form.initial['harga'] = hasil[0].harga
        # form.initial['status'] = hasil[0].status
        # form.initial['ukuran'] = hasil[0].ukuran
        # form.initial['kondisi'] = hasil[0].kondisi
        if(request.method == 'POST' and form.is_valid):
            form = UpdateForm(request.POST, instance = hasil)
            form.save()
            return redirect('/barang/detailBarang/'+ str(pk))
        else:
            msg = 'Gagal!'
        return render(request, 'updateBarang.html', {'form' : form})
    else:
        return redirect('/')

def detailBarang(request, pk):
    if request.user.is_authenticated:
        detail = Barang.objects.filter(idBarang = pk).first()
        return render(request, 'detailBarang.html', {'detail' : detail})
    else:
        return redirect('/')

def deleteBarang(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            if request.GET.get('idBarang') is not None:
                Barang.objects.filter(idBarang = request.GET.get('idBarang')).delete()
        # listB = Barang.objects.all()
        # p = request.user
        # listBarang = Barang.objects.filter(penjual = p)
        return redirect('/barang/listBarang')
    else:
        return redirect('/')

