from django.urls import path
from . import views
app_name = 'authentication'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('register/', views.formRegister, name='formRegister'),
    path('registerPenjual/', views.registerPenjual, name='registerPenjual'),
    path('registerPembeli/', views.registerPembeli, name='registerPembeli'),
    path('login/', views.login_view, name='login_view'),
    path('home/', views.home, name='home'),
    path('logout/', views.logout_view, name='logout_view'),
]