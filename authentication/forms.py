from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User

class LoginForm(forms.Form):
    username = forms.CharField(
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
            }
        )
    )

class SignUpPenjualForm(UserCreationForm):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                'pattern':'[A-Za-z ]+', 
                'title':'Enter Characters Only '
                
            }
        )
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'is_penjual', 'is_pembeli', 'nama')

class SignUpPembeliForm(UserCreationForm):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                'pattern':'[A-Za-z ]+', 
                'title':'Enter Characters Only '
                
            }
        )
    )
    no_hp = forms.IntegerField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    alamat = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control"
                
            }
        )
    )
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'is_pembeli', 'is_penjual', 'nama', 'no_hp', 'alamat')
