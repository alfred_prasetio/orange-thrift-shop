from django.db import models
from django.conf import settings
from django.db.models.enums import Choices
from authentication.models import User


# Create your models here.
class Transaksi(models.Model):
    pembeli = models.ForeignKey(User, on_delete=models.CASCADE, null= True)
    idTransaksi = models.CharField(blank = False, max_length=30, unique=True)
    metodePembayaran = models.CharField(blank = False, max_length=30)
    totalHarga = models.IntegerField(blank=False)
    ongkosKirim = models.IntegerField(blank=False)
    jenisBarang = models.CharField(blank = False, max_length=30)
    def __str__(self):
        return self.idTransaksi
