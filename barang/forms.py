from django import forms
from django.forms import fields
from .models import Barang

class FormBarang(forms.ModelForm):
    idBarang = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    namaBarang = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    jenisBarang = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    harga = forms.IntegerField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    status = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    ukuran = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    kondisi = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )

    class Meta:
        model = Barang
        fields = ('idBarang', 'namaBarang', 'jenisBarang', 'harga', 'status', 'ukuran', 'kondisi', 'is_terjual', 'pembeli')
    def __init__(self, *args, **kwargs):
        super(FormBarang, self).__init__(*args, **kwargs)
        self.fields['idBarang'].disabled = True

class BaseForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class UpdateForm(BaseForm, forms.ModelForm):
    class Meta:
        model = Barang
        fields = ('idBarang', 'namaBarang', 'jenisBarang', 'harga', 'status', 'ukuran', 'kondisi', 'is_terjual', 'pembeli')
    def __init__(self, *args, **kwargs):
        super(UpdateForm, self).__init__(*args, **kwargs)
        self.fields['idBarang'].disabled = True