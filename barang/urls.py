from django.urls import path
from . import views
app_name = 'barang'

urlpatterns = [
    path('listBarang/', views.readBarang, name ='readBarang'),
    path('createBarang/', views.createBarang, name = 'createBarang'),
    path('updateBarang/<int:pk>', views.updateBarang, name = 'updateBarang'),
    path('detailBarang/<int:pk>/', views.detailBarang, name = 'detailBarang'),
    path('deleteBarang/', views.deleteBarang, name = 'deleteBarang'),
]