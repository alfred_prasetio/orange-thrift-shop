from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from .forms import SignUpPembeliForm, LoginForm, SignUpPenjualForm
from django.conf import settings

# Create your views here.
def landing(request):
    if request.user.is_authenticated:
        return render(request, 'home.html')
    else:
        return render(request, 'landing.html')
    

def formRegister(request):
    return render(request, 'formRegister.html')

def home(request):
    if request.user.is_authenticated:
        return render(request, 'home.html')
    else:
        return redirect('/')

def registerPenjual(request):
    msg = None
    if request.method == 'POST':
        form = SignUpPenjualForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_penjual = True
            user.save()
            msg = 'User created'
            print("valid")
            return redirect('/login')
        else:
            print("tidak")
            msg = 'Registration Failed, Please Try Again'
    else:
        form = SignUpPenjualForm()
    return render(request,'registerPenjual.html', {'form': form, 'msg': msg})

def registerPembeli(request):
    msg = None
    if request.method == 'POST':
        form = SignUpPembeliForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_pembeli = True
            user.save()
            msg = 'User created'
            print("valid")
            return redirect('/login')
        else:
            print("tidak")
            msg = 'Registration Failed, Please Try Again'
    else:
        form = SignUpPembeliForm()
    return render(request,'registerPembeli.html', {'form': form, 'msg': msg})

def login_view(request):
    form = LoginForm(request.POST or None)
    msg = None
    if request.method == 'POST':
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_penjual:
                login(request, user)
                request.session['role'] = 'penjual'
                return redirect('/home')
                # return render(request,'main/profilDosen.html')
            elif user is not None and user.is_pembeli:
                login(request, user)
                request.session['role'] = 'pembeli'
                return redirect('/home')
                # return render(request,'main/profilMahasiswa.html')
            else:
                msg= 'Invalid Credentials'
        else:
            msg = 'Error Validating Form'
    return render(request, 'login.html', {'form': form, 'msg': msg})

def logout_view(request):
    logout(request)
    return redirect('/')