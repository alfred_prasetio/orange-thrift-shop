from django import forms
from django.forms import fields
from .models import Transaksi

metode_choices = [
    ('Transfer Bank', 'Transfer Bank'),
    ('COD', 'COD'),
]
class FormTransaksi(forms.ModelForm):
    idTransaksi = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    metodePembayaran = forms.ChoiceField(
        required=True,
        choices=metode_choices,
    )
    totalHarga = forms.IntegerField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    ongkosKirim = forms.IntegerField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    jenisBarang = forms.CharField(
        required=True,
        widget= forms.TextInput(
            attrs={
                "class": "form-control"
            }
        )
    )

    class Meta:
        model = Transaksi
        fields = ('idTransaksi', 'metodePembayaran', 'totalHarga', 'ongkosKirim', 'jenisBarang')

    def __init__(self, *args, **kwargs):
        super(FormTransaksi, self).__init__(*args, **kwargs)
        self.fields['totalHarga'].disabled = True
        self.fields['ongkosKirim'].disabled = True
        self.fields['jenisBarang'].disabled = True
        self.fields['idTransaksi'].disabled = True