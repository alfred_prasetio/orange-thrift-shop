from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class User(AbstractUser):
    is_penjual = models.BooleanField('Is Penjual', default = False)
    is_pembeli = models.BooleanField('Is Pembeli', default = False)
    email = models.EmailField(max_length = 30, blank = False)
    nama = models.CharField(max_length=30, blank = False)

